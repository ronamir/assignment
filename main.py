import requests
import tarfile
import os
import sys
from shutil import copyfile
import pandas as pd

import pydicom
from pydicom.data import get_testdata_files
from pydicom.filereader import read_dicomdir


DICOMS_DIR = 'DICOMS'
PATIENTS_DIR = 'PATIENTS'
DELIMITER = '-' * 70


######################################################################################################################################################
#													DOWNLOADING AND EXTRACTIGN THE FILES
######################################################################################################################################################

def get_agrs():
	if len(sys.argv) != 2:
		print 'The only given argument should be URL to the file. Exiting the program'
		exit(1)
	else:
		return sys.argv[1]

def download_file(url):
	filename = url.split("/")[-1]
	with open(filename, "wb") as f:
		r = requests.get(url)
		f.write(r.content)
	return filename

def extract_tgz(filename):
    tar = tarfile.open(filename, 'r')
    for item in tar:
        tar.extract(item, DICOMS_DIR)
        if item.name.find(".tgz") != -1 or item.name.find(".tar") != -1:
            extract(item.name, "./" + item.name[:item.name.rfind('/')])


######################################################################################################################################################
#															PARSINGTHE THE DICOM FILES
######################################################################################################################################################


def sort_metadata():
	'''
	This function reads all .dcm files and indexes their metadata in a DataFrame
	'''
	all_dicoms = pd.DataFrame(columns=['PatientName', 'StudyInstanceUID', 'SeriesInstanceUID', 'PatientAge', 'PatientSex', 'InstitutionName', 'ContentTime'])
	for filepath in pydicom.data.data_manager.get_files(DICOMS_DIR, '*.dcm'):
		try:
			ds = pydicom.dcmread(filepath, force=True)
			all_dicoms = all_dicoms.append({
				'PatientName': ds.PatientName, 
				'StudyInstanceUID': ds.StudyInstanceUID,
				'SeriesInstanceUID': ds.SeriesInstanceUID,
				'PatientAge': ds.PatientAge,
				'PatientSex': ds.PatientSex,
				'InstitutionName': ds.InstitutionName,
				'filename': filepath,
				'ExposureTime': ds.ExposureTime,
				'ContentTime': ds.ContentTime
			}, ignore_index=True)
		except Exception as e:
			print "ERROR occoured in reading dicom files: {}".format(str(e))

	return all_dicoms


def arrange_in_directories(dicoms_df):
	'''
	a function that saves all the dicom files in an organized hirarchical tree
	'''
	os.mkdir(PATIENTS_DIR)
	for patient in dicoms_df['PatientName'].unique():
		patient_path = os.path.join(PATIENTS_DIR, patient)
		os.mkdir(patient_path)

		patient_events = dicoms_df[dicoms_df['PatientName'] == patient]
		for study in patient_events['StudyInstanceUID'].unique():
			study_path = os.path.join(patient_path, study)
			os.mkdir(study_path)

			study_events = patient_events[dicoms_df['StudyInstanceUID'] == study]
			for series in study_events['SeriesInstanceUID'].unique():
				series_path = os.path.join(study_path, series)
				os.mkdir(series_path)

				for filename in patient_events[dicoms_df['SeriesInstanceUID'] == series]['filename']:
					copyfile(filename, os.path.join(series_path, os.path.split(filename)[1]))


def analysis_method(dicoms_df):
	'''
	This function prints statistics about the scans data
	'''
	# patients information
	print '\n', DELIMITER
	print 'All Patients Are:'
	print dicoms_df[['PatientName', 'PatientSex', 'PatientAge']].drop_duplicates()

	# hospitals
	print DELIMITER
	hospitals = dicoms_df['InstitutionName'].unique()
	print '\nThere are {} different hospitals which are: <{}>'.format(len(hospitals), ', '.join(hospitals))

	# avg scan time
	group = dicoms_df.groupby('SeriesInstanceUID')
	maxs = group.max()['ContentTime']
	mins = group.min()['ContentTime']
	scan_lengths = pd.DataFrame([float(maxs[k]) - float(mins[k]) for k in maxs.keys()])

	print DELIMITER
	print '\na CT scan takes on everage {} sec'.format(scan_lengths.mean()[0])




def main():
	url = get_agrs()
	filename = download_file(url)
	extract_tgz(filename)
	df = sort_metadata()
	arrange_in_directories(df)
	analysis_method(df)


if __name__ == '__main__':
	main()