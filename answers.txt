The script prints to the command line all the answers to the question.
It also creates 2 directories named "DICOMS" and "PATIENTS" (configurable) where the first one conatins all the .dcm files not organized, and the second is the hiracrchical tree.

I used pandas module to work on the data parsed from the pydicom functions.
* to find the avg time for a CT scan, I ran "dir" on the object reurned from pydicom.dcmread function, and saw the attribute "ContentTime", which was the only Time attribute I saw
  that changes within a single series. To calculate the duration of each series, I used "groupby" method (by SeriesInstanceUID), and for each series - found the gap between the maximal ContentTime
  in the series and the minimal time (the list saved at scan_lengths). The last thing to do was to run a mean function over this list.
  
The differnce between the 2 series of patient 1.2.840.113619.2.337.3.2831186181.801.1414550448.623:
 - When viewing the pictures I noticed that the second series(thin plane) is far more detailed than the first (5/5mm).
   From my understanding of the pictures, each picture is a horizontal cut of the head (seeing the eyes gets bigger, and than smaller, and also the brain...), and I can guess that 
   when the doctors want to see more details they make a more "condensed" scan - which moves in smaller gaps (256 pics on the thin vs. 32 pics on the 5/5mm). With the new photos, 
   they can see more clearly and more percisley.
 - After seeing all the pictures and getting a sense of the brain's general shape, I've noticed that in some of the cases there are asymmetric "stains" (marked in red) on one
   side of the brain, which seemed to me very odd - because I would expect the human brain to be symmetric in general.